package scalars

import (
	"database/sql"
	"github.com/99designs/gqlgen/graphql"
)

// MarshalNullString returns graphql.Null if string is NULL. If not, then uses default string implementation.
func MarshalNullString(ns sql.NullString) graphql.Marshaler {
	if !ns.Valid {
		return graphql.Null
	}
	return graphql.MarshalString(ns.String)
}

func UnmarshalNullString(v interface{}) (sql.NullString, error) {
	if v == nil {
		return sql.NullString{Valid: false, String: ""}, nil
	}

	s, err := graphql.UnmarshalString(v)
	return sql.NullString{String: s, Valid: err == nil}, err
}
