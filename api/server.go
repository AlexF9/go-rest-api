package api

import (
	"bitbucket.org/AlexF9/go-rest-api/api/router"
	"bitbucket.org/AlexF9/go-rest-api/config"
	"fmt"
	_ "github.com/lib/pq"
	"log"
)

/*type Product struct {
	id          sql.NullInt32
	name        sql.NullString
	addition    sql.NullString
	addition_2  sql.NullString
	size        sql.NullString
	desciption  sql.NullString
	category_id sql.NullInt32
	created_at  sql.NullString
	updated_at  sql.NullString
	vendor_id   sql.NullInt32
	image       sql.NullString
}*/

func init() {
	config.Load()
}

func Start() {
	fmt.Printf("\n\tListening [::]:%d", config.PORT)
	listen(config.PORT)
}

func listen(port int) {
	r := router.New()
	log.Fatal(r.Run(fmt.Sprintf(":%d", port)))
}
