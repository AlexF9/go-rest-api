package controllers

import (
	"bitbucket.org/AlexF9/go-rest-api/api/database"
	"bitbucket.org/AlexF9/go-rest-api/api/responses"
	"fmt"
	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
	"net/http"
)

type ProductController struct{}

func (s ProductController) GetAll(c *gin.Context) {

	db, err := database.Connect()
	if err != nil {
		responses.ERROR(c.Writer, http.StatusInternalServerError, err)
		return
	}
	defer db.Close()
	//db, err := sql.Open("postgres", "host=localhost dbname=postgres sslmode=disable user=postgres")

	row := db.QueryRow("SELECT *From get_products();")

	var jsonData []byte
	if err := row.Scan(&jsonData); err != nil {
		panic(err)
	}

	fmt.Println(jsonData)

	c.Data(200, "application/json", jsonData)
}

/* func (con ProductController) GetAll(c *gin.Context) {
	db, err := sql.Open("postgres", "host=localhost dbname=postgres sslmode=disable user=postgres")

	rows, err := db.Query("SELECT *FROM get_products()")
	if err != nil {
		panic(err)
	}
	cols, err := rows.Columns()
	if err != nil {
		panic(err)
	}
	// we"ll want to end up with a list of name->value maps, a la JSON
	// surely we know how many rows we got but can"t find it now
	allgeneric := make([]map[string]interface{}, 0)
	// we"ll need to pass an interface to sql.Row.Scan
	colvals := make([]interface{}, len(cols))
	for rows.Next() {
		colassoc := make(map[string]interface{}, len(cols))
		// values we"ll be passing will be pointers, themselves to interfaces
		for i, _ := range colvals {
			colvals[i] = new(interface{})
		}
		if err := rows.Scan(colvals...); err != nil {
			panic(err)
		}
		for i, col := range cols {
			colassoc[col] = *colvals[i].(*interface{})
		}
		allgeneric = append(allgeneric, colassoc)
	}

	err2 := rows.Close()
	if err2 !=nil {
		panic(err2)
	}

	fmt.Println(allgeneric)

	c.JSON(200, colvals)

}*/
