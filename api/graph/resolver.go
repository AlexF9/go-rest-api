package graph

import (
	"bitbucket.org/AlexF9/go-rest-api/api/models"
	"database/sql"
)

// This file will not be regenerated automatically.
//
// It serves as dependency injection for your app, add any dependencies you require here.

type Resolver struct {
	db       *sql.DB
	product  *models.Product
	products []*models.Product
}

/*
func (r *queryResolver) Products(ctx context.Context, limit *int, offset *int) ([]*models.Product, error) {
	db, err := database.Connect()
	if err != nil {
		errors.DebugPrintf(err)
		return nil, errors.InternalServerError
	}
	defer db.Close()

	products := make([]*models.Product, 0)

	//var product *models.Product
	//var products []*models.Product

	rows, err := database.LogAndQuery(db, "SELECT id, name, description, addition, size FROM products ORDER BY created_at desc limit $1 offset $2", limit, offset)
	if err != nil {
		panic(err)
	}
	for rows.Next() {
		product := new(models.Product)
		if err := rows.Scan(&product.ID, &product.Name, &product.Description, &product.Addition, &product.Size, &product.Logo, &product.Image); err != nil {
			errors.DebugPrintf(err)
			return nil, errors.InternalServerError
		}
		products = append(products, product)
		//fmt.Println(len(products))
	}

	fmt.Println(len(products))

	return products, nil
}

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

type queryResolver struct{ *Resolver } */
