package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"bitbucket.org/AlexF9/go-rest-api/api/errors"
	"context"
	"fmt"

	"bitbucket.org/AlexF9/go-rest-api/api/database"
	"bitbucket.org/AlexF9/go-rest-api/api/graph/generated"
	"bitbucket.org/AlexF9/go-rest-api/api/models"
)

func (r *queryResolver) Products(ctx context.Context, limit *int, offset *int) ([]*models.Product, error) {
	db, err := database.Connect()
	if err != nil {
		errors.DebugPrintf(err)
		return nil, errors.InternalServerError
	}
	defer db.Close()

	products := make([]*models.Product, 0)

	//var product *models.Product
	//var products []*models.Product

	rows, err := database.LogAndQuery(db, "SELECT id, name, description, addition, size, logo, image FROM products ORDER BY created_at desc limit $1 offset $2", limit, offset)
	if err != nil {
		panic(err)
	}

	for rows.Next() {
		product := new(models.Product)
		if err := rows.Scan(&product.ID, &product.Name, &product.Description, &product.Addition, &product.Size, &product.Logo, &product.Image); err != nil {
			errors.DebugPrintf(err)
			return nil, errors.InternalServerError
		}
		products = append(products, product)
		//fmt.Println(products)
	}

	fmt.Println(len(products))

	//fmt.Println(products)

	return products, nil
}

func (r *queryResolver) Search(ctx context.Context, name *string) ([]*models.Product, error) {
	db, err := database.Connect()
	if err != nil {
		errors.DebugPrintf(err)
		return nil, errors.InternalServerError
	}
	defer db.Close()

	products := make([]*models.Product, 0)

	//var product *models.Product
	//var products []*models.Product

	rows, err := database.LogAndQuery(db, "SELECT id, name, description, addition, size, logo, image FROM products WHERE name LIKE $1 ORDER BY created_at desc", name)
	if err != nil {
		panic(err)
	}

	for rows.Next() {
		product := new(models.Product)
		if err := rows.Scan(&product.ID, &product.Name, &product.Description, &product.Addition, &product.Size, &product.Logo, &product.Image); err != nil {
			errors.DebugPrintf(err)
			return nil, errors.InternalServerError
		}
		products = append(products, product)
		//fmt.Println(products)
	}

	fmt.Println(len(products))

	//fmt.Println(products)

	return products, nil
}

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

type queryResolver struct{ *Resolver }
