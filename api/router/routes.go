package router

import (
	"bitbucket.org/AlexF9/go-rest-api/api/controllers"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func New() *gin.Engine {

	ProductController := controllers.ProductController{}
	graphQLController := controllers.GraphQLController{}

	r := gin.Default()
	r.Use(cors.Default())

	r.POST("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})

	// Products
	r.POST("/products", ProductController.GetAll)

	r.POST("/query", graphQLController.Query())
	r.GET("/", graphQLController.Playground())

	//log.Printf("connect to http://localhost:%s/ for GraphQL playground", port)
	//log.Fatal(http.ListenAndServe(":"+port, nil))

	return r
}
