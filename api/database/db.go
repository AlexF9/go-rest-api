package database

import (
	"bitbucket.org/AlexF9/go-rest-api/config"
	"database/sql"
	"fmt"
	"log"
)

// Connect to the DATABASE
func Connect() (*sql.DB, error) {
	db, err := sql.Open(config.DBDRIVER, config.DBURL)
	if err != nil {
		fmt.Printf("Cannot connect to %s database", config.DBDRIVER)
		log.Fatal("This is the errors:", err)
	} else {
		fmt.Printf("We are connected to the %s database", config.DBDRIVER)
	}
	return db, err
}
func LogAndQuery(db *sql.DB, query string, args ...interface{}) (*sql.Rows, error) {
	fmt.Println(query)
	return db.Query(query, args...)
}

func MustExec(db *sql.DB, query string, args ...interface{}) {
	_, err := db.Exec(query, args...)
	if err != nil {
		panic(err)
	}
}
